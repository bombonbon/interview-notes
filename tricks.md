# Tricks

## File Storage:

- Split between file storage and metadata storage => fast 

- Only sync delta to save bandwidth

- Offline metadata allows us to do offline updates + avoid round trip

- To transfer quickly: break files into smaller chunks

- How can clients efficiently listen to changes happening with other clients?
  
  - Polling is bad: waste of bandwidth, delay, not scalable
  
  - Answer: long polling

- This problem require ACID-ity, therefore RSBMS is prob better choice for metadata storage

- Data deduplication: 
  
  - calculate hash (SHA-256) of a chunk when there's an update, find on storage if we have a chunk with the same SHA

- Data partitioning:
  
  - Vertical partition: watch out for join, as it'll be more expensive. There's potentially still scalability issue
  
  - Range-based (e.g. 1st letter of filename) vs. hash-based
    
    - range-based potentially leads to unbalance
    
    - hash-based: we can use [Consistent Hashing](https://www.educative.io/collection/page/5668639101419520/5649050225344512/5709068098338816)

## Facebook Messenger

- We need a chat server orchestrating the communications

- How to handle sending/receiving messages:
  
  - Maintain connection between device & server: push via HTTP long polling or WebSockets
  
  - How to keep track of user connections: has a hash table with user id as key mapped to connection object as value.
  
  - What happen if user goes offline? Server notify other user that delivery was a failure after certain timeout

- How to store/retrieve messages from database
  
  - RDBMS is a bad choice since it doesn't handle continuously read/write small entries all the time
  
  - NoSQL is better: example [HBase](https://en.wikipedia.org/wiki/Apache_HBase). HBase is very efficient for this kind (store multiple values to the same key) stored as buffer and dump into disk when it's full
  
  - Fetching: maybe pagination to keep the payload small

- How to keep track of online/offline status
  
  - When user start the app, it fetch online statuses of all contacts
  
  - When user sends a message to offline user, we send a failure back, and update the other status
  
  - Whenever a user comes back online, it pings the server to update its status

- How to shard the DB?
  
  - Scheme: shard based on User ID => benefits: fast to display full history

- How to cache?
  
  - Cache most recent converstations

---

# Photo Sharing Service (Instagram)

## 1. What is it?

### Basics

- share photos to other users

- follow other users (newsfeed)

### Advanced

- Share to other services

- public/private

- tagging

- comments

- suggestion who to follow

## 2. Requirements

### Functional

- Able to download/upload/view photos

- Search based on photo titles

- Follow other users

- Newsfeed displays top photos from other users

### Non-functional

- Highly available

- Low latency on the newsfeed generation

- Consistency can take a hit (eventual consistency)

- Highly reliable (photos & videos are not lost)

## 3. Design Considerations

- Think about storage: users should be upload as many photos as they want => Efficient Storage

- Low latency in viewing photos
  
  > These 2 combined => separate between metadata store & object store?

- High reliability: when user upload a photo, it must not be lost

## 4. Storage

- Store photos in distributed object store: S3 (high availability, low latency)

- Metadata can be stored in:
  
  - RDBMS (since we joins between photos and users), or...
  
  - Distributed key - value store (key is photoID, value contains metadata: photo location, user id, creation timestamp, etc.)
    
    > - Cassandra or other key-value store always keep replicas => high availability
    > 
    > - deletes don't get applied immediately => available for certain amount of days

## 5. Components Design

- Service is read heavy. Also, write takes a long time (upload), but read should be faster (via cache)

- Traditionally, if read write came from the same server, if write/upload is busy, read cannot be done. Can be solved by splitting read / write to different servers

## 6. Reliability & Redundancy & Sharding

- Multiple replicas of Image storage

- Backup of metadata storage

- Horizontal sharding on users (so that we can keep on photos of the same user on the same shard)
  
  - How do we handle hot users (celebrities)
  
  - If we also shard on photos => high latency?
    
    - If we shard on photos, how can we generate unique ID among shards? **Solution**: separate database to generate unique ID, each shard will get next available ID from this database
      
      - This **is** single point of failures. **Solutions:** 2 databases:1 for odd IDs, 1 for even IDs, and have a load-balancer sitting in front of it
      - Since we frequently sort latest photos 1st, we may concat this unique id with epoch timestamp as the ID

## 7. Scalability

- Move database partition
  - [ ] Need to read more about this

## 8. Ranking & News Feed Generation

- Example: Top 100 photos from user's news feed
  
  - Algorithm: query followed users, fetch latest 100 photos each, send to a Ranking algorithm. **Problems:** high latency
  
  - **Solution to latency:** pre-generating news feed (from a separate service, e.g. `NewsFeedGenerator`, store in a separate table `UserNewsFeed`)

- How to deliver news feed to the users?
  
  - Pull: users can manually request latest news feed. **Issues:**   
    
    - New data not available until user requests
    
    - Most of the time pull request will result in empty data
  
  - Push: how do we establish connection? 
    
    - [Long Poll](https://en.wikipedia.org/wiki/Push_technology#Long_polling)
  
  - Hybrid: push for less-traffic users, and pull for more-traffic users

## 9. Cache & Load balancing

- Location awareness: maybe CDN?

- Memcache to cache metadata
